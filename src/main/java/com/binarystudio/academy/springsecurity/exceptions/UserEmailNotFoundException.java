package com.binarystudio.academy.springsecurity.exceptions;

public class UserEmailNotFoundException extends RuntimeException {
    public UserEmailNotFoundException(String message) {
        super(message);
    }

    public UserEmailNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
