package com.binarystudio.academy.springsecurity.domain.user;

import com.binarystudio.academy.springsecurity.domain.user.model.User;
import com.binarystudio.academy.springsecurity.exceptions.UserEmailNotFoundException;
import com.binarystudio.academy.springsecurity.exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("Not found"));
    }

    //	public List<User> getAll() {
//		return userRepository.findUsers();
//	}
    public List<UserDto> getAll() {
        return userRepository
                .findUsers()
                .stream()
                .map(UserDto::fromEntity)
                .collect(Collectors.toList());
    }

    public User createUser(String login, String email, String password) {
        return userRepository.createUser(login, email, password);
    }

    public User updateUserPassword(User user, String newPassword) {
        return userRepository.updatePassword(user, newPassword).orElseThrow(() -> new UserNotFoundException("User not found"));
    }

    public User getUserByEmail(String email) {
        return userRepository.getByEmail(email).orElseThrow(() -> new UserEmailNotFoundException("Email not found"));
    }
}
