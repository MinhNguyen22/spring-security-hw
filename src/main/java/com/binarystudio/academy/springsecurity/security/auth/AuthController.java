package com.binarystudio.academy.springsecurity.security.auth;

import com.binarystudio.academy.springsecurity.domain.user.model.AuthorizationRequest;
import com.binarystudio.academy.springsecurity.domain.user.model.User;
import com.binarystudio.academy.springsecurity.security.auth.model.*;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("auth")
public class AuthController {
	private final AuthService authService;

	public AuthController(AuthService authService) {
		this.authService = authService;
	}

	@PostMapping("login")
	public AuthResponse login(@RequestBody AuthorizationRequest authorizationRequest) {
		return authService.performLogin(authorizationRequest);
	}

	@GetMapping("me")
	public User whoAmI(@AuthenticationPrincipal User user) {
		return user;
	}

	@PostMapping("register")
	public AuthResponse register(@RequestBody RegistrationRequest registrationRequest) {
		return authService.performRegistration(registrationRequest);
	}

	@PostMapping("refresh")
	public AuthResponse refreshTokenPair(@RequestBody RefreshTokenRequest refreshTokenRequest) {
		return authService.performRefreshTokenPair(refreshTokenRequest);
	}

	@PutMapping("forgotten_password")
	public void forgotPasswordRequest(@RequestParam String email) {
		authService.performEmailConfirmation(email);
	}

	@PatchMapping("forgotten_password")
	public AuthResponse forgottenPasswordReplacement(@RequestBody ForgottenPasswordRequest forgottenPasswordReplacementRequest) {
		return authService.performPasswordReplacement(forgottenPasswordReplacementRequest);
	}

	@PatchMapping("change_password")
	public AuthResponse changePassword(@AuthenticationPrincipal User user, @RequestBody PasswordChangeRequest passwordChangeRequest) {
		return authService.performChangingPassword(user, passwordChangeRequest);
	}
}
